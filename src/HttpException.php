<?php declare(strict_types=1);

namespace Lliure\Http\Exception;

class HttpException extends \Exception implements HttpExceptionInterface
{

    protected array $headers = [];

    /** @var string */
    protected $message;

    protected int $status;

    /**
     * Constructor.
     *
     * @param int        $status
     * @param string     $message
     * @param \Exception $previous
     * @param array      $headers
     * @param int        $code
     */
    public function __construct(
        int        $status,
        string     $message = null,
        \Exception $previous = null,
        array      $headers = [],
        int        $code = 0
    ) {
        $this->headers = $headers;
        $this->message = $message;
        $this->status  = $status;

        parent::__construct($message, $code, $previous);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusCode(): int
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
