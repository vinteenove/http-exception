<?php

namespace Lliure\Http\Exception;

class NotFound extends HttpException{

    public function __construct(
        int $status = 404,
        string $message = 'Not Found',
        \Exception $previous = null,
        array $headers = [],
        int $code = 0
    ){
        parent::__construct($status, $message, $previous, $headers, $code);
    }

}