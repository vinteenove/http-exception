<?php

namespace Lliure\Http\Exception;

class MethodNotAllowed extends HttpException{

    public function __construct(
        int $status = 405,
        string $message = 'Method Not Allowed',
        \Exception $previous = null,
        array $headers = [],
        int $code = 0
    ){
        parent::__construct($status, $message, $previous, $headers, $code);
    }

}