<?php


namespace Lliure\Http\Exception;


class BadRequest extends HttpException{

    public function __construct(
        int $status = 400,
        string $message = 'Bad Request',
        \Exception $previous = null,
        array $headers = [],
        int $code = 0
    ){
        parent::__construct($status, $message, $previous, $headers, $code);
    }

}